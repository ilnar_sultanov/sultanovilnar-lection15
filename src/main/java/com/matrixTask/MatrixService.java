package com.matrixTask;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Метод sum декомпозирует работу путем запуска задач ColumnSummator, которые
 * реализуют интерфейс Callable<Integer>.
 *
 * Декомпозированные задачи ColumnSummator запускать в ExecutorService.
 * Запрещено использовать wait-notify
 */
public class MatrixService {

    private static final Queue<Integer> resultQueue = new LinkedBlockingQueue<>();

    public int sum(int[][] matrix, int nThreads){

        ExecutorService service = Executors.newFixedThreadPool(nThreads);

        CountDownLatch latch = new CountDownLatch(nThreads);

        List<FutureTask<Integer>> futureTaskList = new ArrayList<>();

        List<Integer> numbers = Arrays.stream(matrix)
                .flatMap(x -> Arrays.stream(x).boxed())
                .collect(Collectors.toList());

        int len = numbers.size();


        System.out.println("Waiting for calculate processes to complete....");

        if (len % nThreads == 0){
            int rowsForThread = len / nThreads;

            for (int i = 0; i < len; i += rowsForThread){
                FutureTask<Integer> futureTask = new FutureTask<>(
                        new ColumnSummator(numbers.subList(i, i + rowsForThread)));

                futureTaskList.add(futureTask);
                service.execute(futureTask);
            }
        }
        else {
            int startThreadsRows = len / nThreads;
            int lastThreadRows = startThreadsRows + len % nThreads;

            for (int i = 0; i < len - lastThreadRows; i += startThreadsRows){
                FutureTask<Integer> futureTask = new FutureTask<>(
                        new ColumnSummator(numbers.subList(i, i + startThreadsRows)));

                futureTaskList.add(futureTask);
                service.execute(futureTask);
            }
            FutureTask<Integer> futureTask = new FutureTask<>(
                    new ColumnSummator(numbers.subList(len - lastThreadRows, len)));

            futureTaskList.add(futureTask);
            service.execute(futureTask);
        }

        for (FutureTask<Integer> futureTask : futureTaskList){
            while (true) {
                if (futureTask.isDone()) {
                    try {
                        resultQueue.add(futureTask.get());
                        latch.countDown();
                        break;
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        try {
            latch.await();
            service.shutdown();
            System.out.println("All process completed...");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return resultQueue.stream().mapToInt(x -> x).sum();
    }


    static class ColumnSummator implements Callable<Integer> {

        private final List<Integer> nums;

        public ColumnSummator(List<Integer> nums) {
            this.nums = nums;
        }

        @Override
        public Integer call() {
            return nums.stream().mapToInt(x -> x).sum();
        }
    }
}
