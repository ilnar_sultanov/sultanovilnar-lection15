package com.matrixTask;

import java.util.Random;

public class Main {
    private final static int NUMBER_OF_THREADS = 8;

    public static void main(String[] args) {

        for (int i = 0; i < 1000000; i++) {
            int[][] matrix = getMatrix(10, 20);

            MatrixService matrixService = new MatrixService();
            System.out.println("Sum of numbers of matrix = " + matrixService.sum(matrix, NUMBER_OF_THREADS));
        }
    }

    static int[][] getMatrix(int rows, int cols){
        int[][] matrix = new int[rows][cols];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = new Random().nextInt(100);
            }
        }
        return matrix;
    }
}
